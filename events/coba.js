// 100 derajat celcius = "air mendidih"
// 50 derajat celcius = "air hangat"
// 0 derajat celcius = "air membeku"

const Event = require("events");
const eventEmitter = new Event();

function mendidih(suhu) {
  console.log("Air Mendidih " + suhu);
}

function hangat(suhu) {
  console.log("Air Hangat " + suhu);
}

function beku(suhu) {
  console.log("Air membeku " + suhu);
}

// Registrasi Event
eventEmitter.on("mendidih", mendidih);
eventEmitter.on("hangat", hangat);
eventEmitter.on("beku", beku);

// for (let suhu = 200; suhu > -2; suhu--) {
//   // if (suhu == 100) {
//   //     eventEmitter.emit('mendidih', suhu);
//   // } else if (suhu == 50) {
//   //     eventEmitter.emit('hangat', suhu);
//   // } else if (suhu == 0) {
//   //     eventEmitter.emit('beku', suhu);
//   // }
// }
for (let suhu = 200; suhu > -2; suhu--) {
  switch (suhu) {
    case 100:
      eventEmitter.emit(`${suhu} "air mendidih"`);
      break;

    case 50:
      eventEmitter.emit(`${suhu} "air hangat"`);
      break;

    case 0:
      eventEmitter.emit(`${suhu} "air membeku"`);
      break;

    default:
      break;
  }
}
