const Event = require("events");

// / Inisiasi sebuah variabel event
const  eventEmitter = new Event();

// Inisiasi function handler
function lampuMerah(){
    console.log('kendaraan harus berhenti')
}

function lampuHijau(){
    console.log('kendaraan harus jalan')
}

// Registrasi sebuah event
eventEmitter.on('lampumerah', lampuMerah);
eventEmitter.on('lampuhijau', lampuHijau);

// Trigger event
eventEmitter.emit('lampuhijau');

for (let index = 0; index <= 10; index++) {
    if (index % 2 = 0) {
        eventEmitter.emit("genap", index)
    } else {
        eventEmitter.emit('ganjil', index);
    }
}