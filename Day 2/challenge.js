class Guitar {
  constructor(brand, model, color) {
    this.brand = brand;
    this.person = "John Petrucci";
    this.model = model;
    this.color = color;
  }

  guitarPlay() {
    console.log(`Gitar dimainkan oleh ${this.person}`);
    console.log(`Menggunakan gitar merk ${this.brand}`);
    console.log(`Modelnya ${this.model}`);
    console.log(`Warnanya ${this.color}`);
  }
  // changeGuitar(model, brand, color) {
  //   this.model = model;
  //   this.brand = brand;
  //   this.color = color;
  // }
}
const guitar = new Guitar("Fender", "Stratocaster", "Cream");
const guitar2 = new Guitar("Gibson", "Les Paul", "Merah");
const guitar3 = new Guitar("Ibanez", "RG Series", "Hitam");

guitar.guitarPlay();

guitar2.guitarPlay();

guitar.guitarPlay();
