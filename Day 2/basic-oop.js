// memulai sebuah object
const mobil = {
  // cara membuat properti pada object
  name: "Toyota Avanza",
  weight: 800,
  model: "MPV",
  color: "putih",
  start: function () {
    console.log(`${this.name} mobil telah menyala`);
  },
  drive: function () {
    console.log("mobil siap dikemudikan");
  },
  break: function () {
    console.log("mobil direm");
  },
  stop: function () {
    console.log("mobil berhenti");
  },
};

mobil.name = "honda Jazz";
mobil.name = "Suzuki Ertiga";
mobil.start();

//cara memanggil properti object pertama
//console.log(mobil.model)

//cara memanggil properti object kedua
//console.log(mobil['model'])

//cara memanggil method
// mobil.start()
// mobil.drive()
// mobil.break()
// mobil.stop()

mobil["start"]();
