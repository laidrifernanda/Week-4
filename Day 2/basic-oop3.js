// class umumnya diawali dengan huruf besar

class Mobil {
  // digunakan method pembantu fungsi yang digunakan saat memulai class
  constructor(name, color, weight) {
    this.name = name;
    this.weight = 800;
    this.model = "MPV";
    this.color = color;
  }

  start() {
    console.log(`${this.name} yang warnanya ${this.color} telah menyala`);
  }
  drive() {
    console.log(`${this.name} mobil siap dikemudikan`);
  }
  break() {
    console.log("mobil direm");
  }
  stop() {
    console.log("mobil berhenti");
  }
  // changeName(name) {
  //   this.name = name;
  // }
  // changeWeight(weight) {
  //   this.weight = weight;
  // }
  // changeColor(color) {
  //   this.color = color;
  // }
}

// create new instance
const mobil = new Mobil("Honda Jazz", "merah", 800);
const mobil2 = new Mobil("Suzuki Ertiga", "Hitam");
mobil.name = "Honda Jazz";

mobil.start();
