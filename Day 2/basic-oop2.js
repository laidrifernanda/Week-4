// class umumnya diawali dengan huruf besar

class Mobil {
  // digunakan method pembantu fungsi yang digunakan saat memulai class
  constructor() {
    this.name = "Toyota Avanza";
    this.weight = 800;
    this.model = "MPV";
    this.color = "putih";
  }

  start() {
    console.log(`${this.name} telah menyala`);
  }
  drive() {
    console.log(`${this.name} mobil siap dikemudikan`);
  }
  break() {
    console.log("mobil direm");
  }
  stop() {
    console.log("mobil berhenti");
  }
}

// cara memanggil class harus membuat sebuah variabel terlebih dahulu
//creat new instance
const mobil = new Mobil();

console.log(mobil.name);
mobil.drive;

// create new instance
const mobil = new Mobil();
mobil.name = "Honda Jazz";

mobil.start();
