class Car {
  checkWheel() {
    console.log("ada 4 roda");
  }
}

class Motorcycle {
  checkWheel() {
    console.log("ada 2 roda");
  }
}

class Honda {
  constructor(type, dependency) {
    this.type = type;
    this.car = dependency.car;
    this.motorcycle = dependency.motorcycle;
    this.engine = dependency.engine;
  }
}

class Engine {
  checkEngine() {
    console.log("Engine is fine");
  }
}

const car = new Car();
const motorcycle = new Motorcycle();
const engine = new Engine();

const dependency = { car, motorcycle, engine };

const mobilio = new Honda("Mobilio", dependency);

mobilio.car.checkWheel();
mobilio.motorcycle.checkWheel();
mobilio.engine.checkEngine();
