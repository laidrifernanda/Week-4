// Super Class
class Engine {
  startEngine() {
    console.log(`Mesin ${this.type} menyala`);
  }
  stopEngine() {}
}

// child class / sub class
class Car extends Engine {
  horn() {}
}

// child class / sub class
class Toyota extends Car {
  constructor(type) {
    super();
    this.type = type;
  }
}

const avanza = new Toyota("Avanza");

avanza.startEngine();

// console.log(avanza.type);
// console.log(mobilio.type);
// console.log(xenia.type);
