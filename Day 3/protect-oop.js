class Bank {
  #saldo = 1000;
  getSaldo() {
    console.log(`${this.#saldo}`);
  }
  #setSaldo(saldo) {
    this.#saldo += saldo;
  }
  transaction(type, amount) {
    if (type == "debit") {
      this.#saldo += amount;
    } else {
      this.#saldo -= amount;
    }
  }
}

class Atm extends Bank {
  constructor() {
    super();
  }
  withdraw(amount) {
    this.transaction("Kredit", amount);
  }
  payment(amount) {
    this.transaction("Kredit", amount);
  }
  deposit(amount) {
    this.transaction("Debit", amount);
  }
}

// proteksi object dengan command Object.freeze
const atm = Object.freeze(new Atm());

// tidak bisa diubah nilainya (overriding function)
atm.getSaldo = function () {
  console.log(5000000);
};

atm.getSaldo();
