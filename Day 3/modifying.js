class Car {
  constructor() {
    this.brand = "Suzuki";
  }
  startEngine() {
    console.log("Mesin menyala");
  }
}

const car = new Car();

car.brand = "Honda";
car.startEngine = function () {
  console.log("methodnya aku bajak hahaha");
};
console.log(car.brand);
car.startEngine();
