// Super Class
class Engine {
  startEngine() {
    console.log(`Mesin ${this.type} menyala`);
  }
  stopEngine() {}
}

// child class / sub class
class Car extends Engine {
  horn() {} // can use many class
}

// child class / sub class
class Toyota extends Car {
  constructor(type) {
    super();
    this.type = type;
  }
}

class Honda extends Car {
  constructor(type) {
    super();
    this.type = type;
  }
}

class Daihatsu extends Car {
  constructor(type) {
    super();
    this.type = type;
  }
}

const avanza = new Toyota("Avanza");
const mobilio = new Honda("Mobilio");
const xenia = new Daihatsu("Xenia");

avanza.startEngine();
mobilio.startEngine();
xenia.startEngine();

// console.log(avanza.type);
// console.log(mobilio.type);
// console.log(xenia.type);
