class Bank {
  #saldo = 1000;
  getSaldo() {
    console.log(`${this.#saldo}`);
  }
  #setSaldo(saldo) {
    this.#saldo += saldo;
  }
  transaction(type, amount) {
    if (type == "debit") {
      this.#saldo += amount;
    } else {
      this.#saldo -= amount;
    }
  }
}

class Atm extends Bank {
  constructor() {
    super();
  }
  withdraw(amount) {
    this.transaction("Kredit", amount);
  }
  payment(amount) {
    this.transaction("Kredit", amount);
  }
  deposit(amount) {
    this.transaction("Debit", amount);
  }
}

const atm = new Atm();

// Menambah saldo
// atm.deposit(4000);
// atm.getSaldo();

// Saldo sebelum transaksi
atm.getSaldo();

// Bayar Listrik
atm.payment(500);

// Ambil uang
atm.withdraw(200);

// Setor tunai
atm.deposit(1000);

// Saldo sesudah transaksi
atm.getSaldo();
