class Car {
  // Static property
  static brand2 = "Honda";
  constructor() {
    //   Instance Property
    this.brand = "Toyota";
    this.type = "Honda";
  }
  //   Instance method
  startEngine() {
    console.log("Mobil menyalakan mesin");
  }
  static getBrand2() {
    return Car.brand2;
  }
}

const car = new Car();
console.log(Car.brand2);
console.log(Car.getbrand2);
