// const checkVowel = (string) => {
//   let withoutVowels = " ";
//   for (let i = 0; i < string.length; i++) {
//     if (
//       string[i] == "a" ||
//       string[i] == "i" ||
//       string[i] == "u" ||
//       string[i] == "e" ||
//       string[i] == "o"
//     ) {
//       withoutVowels += "i";
//     } else {
//       withoutVowels += string[i];
//     }
//   }
//   return withoutVowels;
// };

// console.log(checkVowel("babi disembelih pak adang"));

// // alternatif program

// const menggantiHurufVokal = (kalimat) => {
//   let stringKosong = "";
//   const hurufVocal = ["a", "i", "u", "e", "o"];

//   for (let i = 0; i < kalimat.length; i++) {
//     if (hurufVocal.includes(kalimat[i])) {
//       stringKosong += "i";
//     } else {
//       stringKosong += kalimat[i];
//     }
//   }
//   console.log(stringKosong);
// };

// menggantiHurufVokal("saya mau liburan ke bali");

// ternary operator
const menggantiHurufVokal = (kalimat) => {
  let stringKosong = "";
  const hurufVocal = ["a", "i", "u", "e", "o"];

  for (let i = 0; i < kalimat.length; i++) {
    stringKosong += hurufVocal.includes(kalimat[i]) ? "i" : kalimat[i];
  }

  console.log(stringKosong);
};

menggantiHurufVokal("saya mau liburan ke bali");
