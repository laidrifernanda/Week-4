const menghitungLuasSegitiga = require("./segitiga");
const menghitungLuasPersegi = require("./persegi");
const menghitungLuasPersegiPanjang = require("./persegiPanjang");
const menghitungLuasLingkaran = require("./lingkaran");

const cekBilanganPrima = require("./prima");

console.log(menghitungLuasSegitiga(10, 10));
console.log(menghitungLuasPersegi(10));
console.log(menghitungLuasPersegiPanjang(10, 10));
console.log(menghitungLuasLingkaran(10));

console.log(cekBilanganPrima(9));
