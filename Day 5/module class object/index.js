const bangunan = require("./bangunan");

console.log(bangunan.classBangunan.luasLingkaran(10));
bangunan.functionBangunan();

// const classBangunaan = bangunan.classBangunan;
// const functionBangunan = bangunan.functionBangunan;

const { classBangunan, functionBangunan } = require("./bangunan");

console.log(classBangunan.luasLingkaran(10));
functionBangunan();
